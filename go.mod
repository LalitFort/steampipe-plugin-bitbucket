module github.com/turbot/steampipe-plugin-bitbucket

go 1.16

require (
	github.com/ktrysmt/go-bitbucket v0.9.16
	github.com/turbot/steampipe-plugin-sdk v0.2.10
)
